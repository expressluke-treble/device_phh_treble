$(call inherit-product, vendor/aosp/config/common_full_phone.mk)
-include vendor/aosp/build/core/config.mk
-include vendor/aosp/build/core/apicheck.mk

# GSI
CUSTOM_BUILD_TYPE := GSI

# Boot Animation
TARGET_BOOT_ANIMATION_RES := 1080
TARGET_BOOT_ANIMATION_RES_EVO := true
